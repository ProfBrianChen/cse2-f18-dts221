///////////
// Dylan Scheidle
// 11/15/18 CSE 002-311
// Passing Arrays Inside Methods- creates an array, and uses methods to copy it, invert it, and print it out each time

public class lab09{
  
  public static int[] copy(int[] list){
    int[] list2 = new int[list.length];   // declare new array that will just have the same elements in the same spots as the input array
    for (int i=0; i<list.length; i++){
    list2[i] = list[i];
    }
    return list2;
  }
  
  public static void inverter(int[] list){
    int i = 0;
    // flips the components of the array to reverse the order
    for (i = 0; i < list.length/2; i++){
      int change = list[i];
      list[i] = list[list.length-1-i];
      list[list.length-1-i] = change;
    }
  }
  
  public static int[] inverter2(int[] list){
    int[] copy = copy(list);   // declares copy of the input array and uses that to invert it
    for (int i = 0; i < copy.length/2; i++){
      int change = copy[i];
      copy[i] = copy[copy.length-1-i];
      copy[copy.length-1-i] = change;
    }
    return copy;   // makes the copy the output
  }
  
  public static void print(int[] list){
    for (int k=0; k< list.length; k++){  
      System.out.print(list[k]+" ");   // prints the elements of the designated array with spaces between each
    } 
    System.out.println();
  }
  
  public static void main(String[] args){
    int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);   // call inverter method with array0
    print(array0);   // call print method to print inverted array0
    inverter2(array1);   // call inverter2 method with array1
    print(array1);   // print method to print the array 1
    int[] array3 = inverter2(array2);   // make new array3 by calling the inverter2 method with array2
    print(array3);   // print array 3 from the previous method call
  }
}