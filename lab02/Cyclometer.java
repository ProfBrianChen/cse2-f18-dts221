///////////
// Dylan Scheidle 9/6/18
// CSE 002-311
// Cyclometer- This program will measure elapsed time and number of bicycle wheel rotations in order to measure speed and distance of a bicycle.

public class Cyclometer {
        // measure speed and distance of a bicycle
       public static void main(String[] args) {
         //input data
         int secsTrip1=480; // time of trip 1 in seconds
         int secsTrip2=3220; // time of trip 2 in seconds
         int countsTrip1=1561; // number of rotations in first trip
         int countsTrip2=9037; // number of rotations in second trip
         // intermediate variables and output data
         double wheelDiameter=27.0; // diameter of wheelDiameter
         double PI=3.14159; // value of pi symbol
         double feetPerMile=5280;  // number of feet in a mile
         double inchesPerFoot=12;   // number of inches in a foot
         double secondsPerMinute=60;  // number of seconds in a minute
         double distanceTrip1, distanceTrip2, totalDistance;  // names for outputs of distances
         // print the results of each trip
         System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
         System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
         // perform distance calculations
         distanceTrip1=countsTrip1*wheelDiameter*PI;
         // Above gives distance in inches
         // for each count, a rotation of the wheel travels
         // the diameter in inches times PI
         distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
         distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
         totalDistance=distanceTrip1+distanceTrip2;
         //Print out the output data.
         System.out.println("Trip 1 was "+distanceTrip1+" miles");
         System.out.println("Trip 2 was "+distanceTrip2+" miles");
         System.out.println("The total distance was "+totalDistance+" miles");
       }
}