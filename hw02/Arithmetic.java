//// CSE 002 Arithmetic
/// Dylan Scheidle
/// 9/9/2018 CSE 002-311
public class Arithmetic{
  
  public static void main(String args[]){
    /// calculate cost of items bought at a store including PA sales tax of 6%
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    //the tax rate
    double paSalesTax = 0.06;
    double totalCostOfPants;   //total cost of pants
    double totalCostOfShirts;   //total cost of shirts
    double totalCostOfBelts;   //total cost of belts
    double salesTaxPants;   //sales tax charged on pants
    double salesTaxShirts;   //sales tax charged on shirts
    double salesTaxBelts;   //sales tax charged on belts
    double totalCostBeforeTaxes;   //total cost of purchases before taxes
    double totalSalesTax;   //total amount of sales tax
    double totalCost;   //total cost of purchase with sales tax;
    totalCostOfPants = numPants * pantsPrice;   // number of pants times price of pants
    totalCostOfShirts = numShirts * shirtPrice;   // number of shirts times price of shirts
    totalCostOfBelts = numBelts * beltCost;   // number of belts times price of belts
    salesTaxPants = paSalesTax * totalCostOfPants;   //sales tax rate times total cost of pants
    salesTaxShirts = paSalesTax * totalCostOfShirts;   //sales tax rate times total cost of shirts 
    salesTaxBelts = paSalesTax * totalCostOfBelts;   //sales tax rate times total cost of belts
    totalCostBeforeTaxes = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;   //total cost of each item added up
    totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts;   //sales tax on each total item cost added up
    totalCost = totalCostBeforeTaxes + totalSalesTax;   //total cost before taxes plus total sales tax
    salesTaxPants = (int) (salesTaxPants * 100)/100.0;   //round value to two decimal places
    salesTaxShirts = (int) (salesTaxShirts * 100)/100.0;   //round value to two decimal places
    salesTaxBelts = (int) (salesTaxBelts * 100)/100.0;   //round value to two decimal places
    totalSalesTax = (int) (totalSalesTax * 100)/100.0;   //round value to two decimal places
    totalCost = (int) (totalCost * 100)/100.0;   //round value to two decimal places
    // print out calculations of purchase
    System.out.println("Total cost of pants is $"+totalCostOfPants+" ");
    System.out.println("Total cost of shirts is $"+totalCostOfShirts+" "); 
    System.out.println("Total cost of belts is $"+totalCostOfBelts+" "); 
    System.out.println("Sales tax charged on pants is $"+salesTaxPants+" "); 
    System.out.println("Sales tax charged on shirts is $"+salesTaxShirts+" "); 
    System.out.println("Sales tax charged on belts is $"+salesTaxBelts+" "); 
    System.out.println("Total cost of purchase before tax is $"+totalCostBeforeTaxes+" "); 
    System.out.println("Total sales tax is $"+totalSalesTax+" "); 
    System.out.println("Total cost of purchase including sales tax is $"+totalCost+" "); 
    
  }
  
}
