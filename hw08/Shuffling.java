///////////
// Dylan Scheidle
// 11/12/18 CSE 002-311
// Shuffling.-
import java.util.Scanner;
import java.util.Random;

public class Shuffling{
  
  public static void shuffle( String[] list){
    System.out.println("Shuffled");   // prints to signal the shuffled deck in printing
    Random randomize = new Random();
    for(int i = 0; i < 500; i++){   // switch two cards 500 times
      String zero = list[0];   // sets variable for zeroth place of array
      int num = randomize.nextInt(52);   // selects a random place in the array
      list[0] = list[num];   // sets the zeroth place to the randomly selected place's position
      list[num] = zero;   // switches the two cards by replacing the zeroth place with the randomly selected spot
    }
  }
  
  public static String[] getHand( String[] list, int index, int numCards){
    System.out.println("Hand");   // singals that the hand is about to be given
		String[] hand = new String[5];   // declares and sets size of new array
		for (int j =0; j < numCards; j++) {   // chooses the top card of the deck 5 times for the hand
			hand[j] = list[index];
			index--;
		}
		return hand;
  }
  
  public static void printArray( String [] list){
    for (int k=0; k< list.length; k++){  
      System.out.print(list[k]+" ");   // prints the elements of the designated array with spaces between each
    } 
    System.out.println();
  }
  
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};   // fills the array with elements
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};   // fills the array with the ranks of cards
    String[] cards = new String[52];   // declares and sets size of new array
    String[] hand = new String[5];   // declares and sets size of a new array
    // initialize and declare variables
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    // puts together and prints every card of a deck and puts these cards into an array
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    } 
    System.out.println();
    printArray(cards);   // calls the printArray() method with the cards array
    shuffle(cards);   // calls the shuffle() method with the cards array
    printArray(cards);   // calls the printArray method with the cards array which is now shuffled
    // for as long as the user wants, pull the 5 card hand off the top of the deck
    while(again == 1){ 
      hand = getHand(cards,index,numCards);   // creates a hand array by calling the getHand() method
      printArray(hand);   // calls the printArray() method with the hand array
      index = index - numCards;   // makes it so the next to last card is picked off the top after the first
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
}
