///////////
// Dylan Scheidle
// 11/12/18 CSE 002-311
// Remove Elements- fills an array with random integers 0-9 and implements methods that either delete a specific user inputed element of the array or all elements equal to a selected target

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class RemoveElements{
   public static void main(String [] arg){
       Scanner scan=new Scanner(System.in);
       int num[]=new int[10];
       int newArray1[];
       int newArray2[];
       int index,target;
       String answer="";
       do{
           System.out.print("Random input 10 ints [0-9] \n");
           num = randomInput();
           String out = "The original array is: ";
           out += listArray(num);
           System.out.println(out);
           System.out.print("Enter the index ");
           index = scan.nextInt();
           newArray1 = delete(num,index);
           String out1="The output array is ";
           out1+=listArray(newArray1); 
           System.out.println(out1);
           System.out.print("Enter the target value ");
           target = scan.nextInt();
           newArray2 = remove(num,target);
           String out2 = "The output array is ";
           out2+=listArray(newArray2); 
           System.out.println(out2);
           System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
           answer=scan.next();
       }while(answer.equals("Y") || answer.equals("y"));
    }

  public static String listArray(int num[]){
        String out="{";
        for(int j=0;j<num.length;j++){
          if(j>0){
            out+=", ";
          }
          out+=num[j];
        }
        out+="} ";
        return out;
  }
  
  // method to randomly fill an array with integers 0-9
  public static int[] randomInput() {
      int [] Array = new int[10];
      int i =0;
      for(i = 0; i < Array.length; i++) {
      Array[i] = (int)(Math.random() * 9);
      }
      return Array;
  }
    
  // method to delete an element of the array
  public static int[] delete(int[] list, int pos) {
      if (pos >= 10 || pos < 0) {
      System.out.println("The index is not valid");
      return list;
      }
      int [] list1 = new int[9]; //initializing array
      int i = 0;
      int temp = 0;  //declare new variable to do a switch  
      for(i = 0; i < list.length; i++) { //loop through each element 
        if (i == pos) {
        temp = 1;
        }
        else { 
          if (temp == 0) { 
            list1[i]=list[i]; 
          }
          if (temp == 1) {  
            list1[i-1]=list[i]; 
          }
        }   
      }
    return list1;
  }
  
  // method to delete all elements in the array equal to the target
  public static int[] remove(int[] list, int target) {
    int j = 0;
    int count =0;
    int temp = 0;
    for(j = 0; j < list.length; j++) {   
      if (list[j] == target) { 
        count ++; 
      }
    }
    int newList[] = new int[list.length-count]; 
    int i=0;
    for (i=0; i < list.length; i++) { 
      if (list[i] == target) {
        temp++; 
      }
      else {
        newList[i-temp] = list[i]; 
      }
    } 
    return newList; //returning the new array 
  }
}
