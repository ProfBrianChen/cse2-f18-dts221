///////////
// Dylan Scheidle
// 11/12/18 CSE 002-311
// CSE2 Linear- asks user for 15 integers for final grades in ascending order and then uses binary search, linear search, and randomizing to find a user inputed selected integer from the array or switch the array around

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class CSE2Linear{

public static void main(String[] args) { 
  Scanner myScanner = new Scanner(System.in); 
  System.out.println("Enter 15 ascending ints for final grades in CSE2: "); //Prompts user for 15 integers 
  String input = myScanner.nextLine();  // asks user to input grade
  String[] grade = input.split(" ");
  System.out.println(Arrays.toString(grade)); //printing the array 
  int[] intArray = new int[grade.length];
  for(int i = 0; i < grade.length; i++) { 
    intArray[i] = Integer.parseInt(grade[i]);
  }     
  int i=0;
  // ensure that the user inputs the numbers in the correct way
  while (i < intArray.length) { //making sure it is less than the length 
    if (intArray[i] < 0 || intArray[i] >100) { //making sure input is in range
      System.out.println( "Error, integers must be between 1-100");
      break;
    }
    else {
      i++; 
    }
  }
  for (int j = 0; j < (grade.length-1); j++) { 
    if (intArray[j+1] <= intArray[j]) { //making sure it is ascending
      System.out.println("Error, the integers must be ascending");
      break;
    }
  }
  System.out.println("Enter a grade to search for: "); //prompting asking for the grade to be searched for
  int m = myScanner.nextInt();
  System.out.println(binarySearch(m, intArray)); // calls and prints the results of the binary search method
  System.out.println("Scrambled: " + Arrays.toString(Scramble(intArray))); 
  System.out.println("Enter a grade to search for: "); //prompting asking for another grade to be searched for
  int n = myScanner.nextInt();
  System.out.println(linearSearch(n, intArray)); // calls and prints the results of the linear search method
}  
  
  // method for Binary Search 
  public static String binarySearch(int point, int[] intArray) { 
    int low =0;
    int high;
    int iterate = 0; 
    high = intArray.length-1; 
    while (high >= low ) { 
      iterate+=1; //increase an iteration counter by 1
      int mid = (low + high)/2; 
      if (point < intArray[mid]) { 
        high = mid - 1; 
      }
      else if (point == intArray[mid]) {
        return "Your search was found, with " + iterate + " iteration(s)"; 
      }
      else {
        low = mid + 1;
      }
    }
    return point + "was not found in the list with " + iterate + " iteration(s)";
  } 
  
  // method for Scrambling Array 
  public static int[] Scramble(int[] intArray) {
    int[] temp = new int[15];// declaring a temporary array
    Random randomNum = new Random(); //random number generator
    for (int i=0; i<15; i++){ 
      int k = randomNum.nextInt(15); 
      temp[k] = intArray[i]; //this is using the created array to hold the value of CSE grades
      intArray[i] = intArray[k]; //this is then taking that value and making it the new value
      intArray[k] = temp[k]; //And this is transferring it to the temporary array 
    }
    return intArray;
  } 
  
  // method for Linear Search 
  public static String linearSearch(int point, int[] intArray) {  
    int iterate = 0; 
    for (int i = 0; i < intArray.length; i++) {
      iterate+=1; //increase an iteration counter by 1
      if (point == intArray[i]) 
        return "Your search was found, with " + iterate + " iteration(s)";
    }
    return point + "was not found in the list with " + iterate + " iteration(s)";
  }
 
}
