///////////
// Dylan Scheidle
// 9/20/18 CSE 002-311
// Card Generator- Program randomly selects a card from a deck and prints the type of card and the suit.
public class CardGenerator{
                // main method required for every Java program
               public static void main(String[] args) {
                 //Generate a random number between 0 and 52
                 int cardNumber = (int)(Math.random()*52)+2;                 
                 String suit = "";   // assign parts of the deck to different decks
                 if(cardNumber >= 1 && cardNumber <= 13) {
                   suit = "Diamonds";   // cards 1 through 13 are diamonds
                 } else if(cardNumber >= 14 && cardNumber <= 26) {
                   suit = "Clubs";   // cards 14 through 26 are clubs
                 } else if(cardNumber >= 27 && cardNumber <= 39) {
                   suit = "Hearts";   // cards 27 through 39 are hearts
                 } else if (cardNumber >= 40 && cardNumber <= 52) {
                   suit = "Spades";   // cards 40 through 52 are spades
                 }
                 // assign cards 1-13 in each suit to a card value
                 int value = (int)(cardNumber%13);
                 String cardValue = "";
                 switch(value){
                   case 1:
                     cardValue = "Ace";
                     break;
                   case 2:
                     cardValue = "2";
                     break;
                   case 3:
                     cardValue = "3";
                     break;
                   case 4:
                     cardValue = "4";
                     break;
                   case 5:
                     cardValue = "5";
                     break; 
                   case 6:
                     cardValue = "6";
                     break;
                   case 7:
                     cardValue = "7";
                     break;
                   case 8:
                     cardValue = "8";
                     break;
                   case 9:
                     cardValue = "9";
                     break;
                   case 10:
                     cardValue = "10";
                     break;
                   case 11:
                     cardValue = "Jack";
                   case 12:
                     cardValue = "Queen";
                     break;
                   case 13:
                     cardValue = "King";
                     break;
                 }
                 // print out the randomly selected card
                 System.out.println("You picked the " + cardValue + " of " + suit);
                 }
}