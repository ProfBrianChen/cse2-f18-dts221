///////////
// Dylan Scheidle
// 10/22/18 CSE 002-311
// Encrypted X- Asks user for input on on size of square, and then prints out a square of stars with a secret X in it
import java.util.Scanner;
public class EncryptedX{
  
      public static void main(String[] args){
        Scanner myScanner = new Scanner ( System.in );
        boolean correct = true;
        System.out.println("Enter the size of a square between 1 and 100:");   // Queue the user to input the number of rows in the pyramid for the pattern
        int input = 0;   // declare variable for number of loops/size of square
        if (myScanner.hasNextInt()){
          input = myScanner.nextInt(); // sets input equal to variable for number of loops/size of square
        } else {
          correct = false; // tests whether the input is the correct type 
        }
        while(!correct){ //while the type is not correct, the program will tell the user there is an error and then will reask the question until the user inputs the correct type 
          myScanner.next();
          System.out.println("Error: wrong type! Enter the number of rows in the pyramid as an integer." ); //tells user there is an error with their type and asks them for new input
          if (myScanner.hasNextInt()){ // checking to see if input is the right type 
            input = myScanner.nextInt(); 
            correct = true; // if its true, it breaks out of the while loop 
          } 
        }
        while (input < 1 || input > 100){
          System.out.println("Error: integer is out of range! Enter an integer between 1 and 100.");
          input = myScanner.nextInt();
        }
    
        // Print the square of stars with the secret X; "Encrypt the X" 
        int row;
        int column;
        for (row = 0; row < input; row++){ 
          for(column = 0; column < input; column++){
            if(row == column || row == (input - (column + 1))){  // makes sure the correct spots are empty for the X's and the rest are stars
              System.out.print(" ");
            } else {
              System.out.print("*");
            }
           }
          System.out.println();  // next line
        }
      }
}
