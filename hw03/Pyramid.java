///////////
// Dylan Scheidle
// 9/16/18 CSE 002-311 HW03 Program 2
// Pyramid- Program asks user for input of dimensions of a pyramid and outputs the volume of the pyramid
import java.util.Scanner;
public class Pyramid{
                // main method required for every Java program
               public static void main(String[] args) {
                 Scanner myScanner = new Scanner( System.in ); //scanner to initiate input
                 System.out.print("The square side of the pyramid is (input length): "); //cue user to input length of side of a pyramid
                 double squareSide = myScanner.nextDouble(); // scanner to accept input
                 System.out.print("The height of the pyramid is (input height): "); //cue user to input height of a pyramid
                 double height = myScanner.nextDouble(); // scanner to accept input
                 double sideSquared = Math.pow(squareSide, 2); // side of pyramid squared is used in volume of pyramid formula
                 double volumeOfPyramid = (sideSquared * height)/3;   // volume of pyramid formula
                 System.out.println("The volume inside the pyramid is: " + volumeOfPyramid);   // prints the volume of the pyramid
                 
               }
  
}