///////////
// Dylan Scheidle
// 9/16/18 CSE 002-311 HW03 Program 1
// Convert- Program asks user for input of acres of land and inches of rainfall from a hurricane and computes/outputs the cubic miles
import java.util.Scanner;
public class Convert{
                // main method required for every Java program
               public static void main(String[] args) {
                 Scanner myScanner = new Scanner( System.in ); //scanner to initiate input
                 System.out.print("Enter the the affected area in acres: "); //cue user to input number of acres
                 double acreage = myScanner.nextDouble(); // scanner to accept input
                 System.out.print("Enter the rainfall in the affected area: "); //cue person to input tip percent
                 double inchesOfRain = myScanner.nextDouble(); //scanner to accept input
                 // 1 acre times 1 inch equals 27,154.2857 gallons
                 double gallonsOfRain = (acreage * inchesOfRain) * 27154.2857;   // calculation to find number of gallons of rainfall
                 double a = Math.pow(10, -13);   // scientific notation for calculation below
                 double cubicMile = (9.08169 * a);   // 1 gallon equals 9.08169 times 10 to the -13th power
                 double cubicMilesOfRain = gallonsOfRain * cubicMile;   // calculation to find number of cubic miles of rainfall from hurricane
                 System.out.println( cubicMilesOfRain + " cubic miles"); //print the answer for cubic miles of rainfall from hurricane
                 
               }
  
}