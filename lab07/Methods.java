///////////
// Dylan Scheidle
// 10/25/18 CSE 002-311
// Methods - Randomly generates integers from 0 to 9 and selects words to create sentences/paragraphs
import java.util.Random;
import java.util.Scanner;

public class Methods{
  
  public static String Adjectives(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);  // Selects a integer less than 10 randomly
    String adj = "";
    switch(randomInt){
      case 0:
        adj = "grumpy";
        break;
      case 1:
        adj = "quick";
        break;
      case 2:
        adj = "tall";
        break;
      case 3:
        adj = "soft";
        break;
      case 4:
        adj = "skinny";
        break; 
      case 5:
        adj = "fancy";
        break;
      case 6:
        adj = "fresh";
        break;
      case 7:
        adj = "hungry";
        break;
      case 8:
        adj = "sweet";
        break;
      case 9:
        adj = "slimy";
        break;
    }
    return adj;
  }
  
  public static String Subjects(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);  // Selects a integer less than 10 randomly
    String sbj = "";
    switch(randomInt){
      case 0:
        sbj = "turtle";
        break;
      case 1:
        sbj = "boy";
        break;
      case 2:
        sbj = "potato";
        break;
      case 3:
        sbj = "zombie";
        break;
      case 4:
        sbj = "puppy";
        break; 
      case 5:
        sbj = "tree";
        break;
      case 6:
        sbj = "hawk";
        break;
      case 7:
        sbj = "farmer";
        break;
      case 8:
        sbj = "car";
        break;
      case 9:
        sbj = "king";
        break;
    }
    return sbj;
  }
  
  public static String Verbs(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);  // Selects a integer less than 10 randomly
    String verb = "";
    switch(randomInt){
      case 0:
        verb = "swallowed the";
        break;
      case 1:
        verb = "jumped on the";
        break;
      case 2:
        verb = "lied to the";
        break;
      case 3:
        verb = "ran to the";
        break;
      case 4:
        verb = "threw the";
        break; 
      case 5:
        verb = "supported the";
        break;
      case 6:
        verb = "carried the";
        break;
      case 7:
        verb = "fought the";
        break;
      case 8:
        verb = "argued with the";
        break;
      case 9:
        verb = "punched the";
        break;
    }
    return verb;
  }
  
  public static String Objects(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);  // Selects a integer less than 10 randomly
    String obj = "";
    switch(randomInt){
      case 0:
        obj = "skyscraper";
        break;
      case 1:
        obj = "arrow";
        break;
      case 2:
        obj = "bottle";
        break;
      case 3:
        obj = "honey badger";
        break;
      case 4:
        obj = "water";
        break; 
      case 5:
        obj = "witch";
        break;
      case 6:
        obj = "spaceship";
        break;
      case 7:
        obj = "bear";
        break;
      case 8:
        obj = "train";
        break;
      case 9:
        obj = "realtor";
        break;
    }
    return obj;
  }
  public static String keepNoun(String sbj) { 
		Random randomGenerator = new Random(); 
		int randInt = randomGenerator.nextInt(2);
		
		if (randInt == 0) {
			sbj = "It ";
  	}
		else {
			sbj = "The " + sbj + " ";
		}
		return sbj; 
	}
	
	public static void Conclusion(String sbj) {
		System.out.print("That " + sbj); 
		System.out.print(" " + Verbs() + " "); 
		System.out.println(Objects() + "!");
	}
	
	public static void finalMethod() {
		
		Random randomGenerator = new Random(); 
		int randInt = randomGenerator.nextInt(3) + 2;
		
		String adj1 = ""; 
		String adj2 = "";
		String sbj1 = "";
		String sbj2 = "";
		String obj1 = "";
		String obj2 = "";
		
		System.out.print("The "); 
		adj1 = Adjectives(); 
		System.out.print(adj1); 
		adj2 = Adjectives(); 
		while (adj1 == adj2) { 
			adj2 = Adjectives(); 
		}
		System.out.print(" " + adj2); 
		sbj1 = Subjects(); 
		System.out.print(" " + sbj1); 
		System.out.print(" " + Verbs()); 
		System.out.println(" " + Objects() + "."); 
		for (int i = 0; i < randInt; i++) {  
		sbj2 = keepNoun(sbj1); 
		System.out.print(sbj2); 
		System.out.print(Verbs()); 
		obj1 = Objects(); 
		System.out.print(" " + obj1);
		System.out.print(" at the"); 
		System.out.print(" " + Adjectives());
		obj2 = Objects();
		while (obj1 == obj2) { 
			obj2 = Objects();
		}
		System.out.println(" " + obj2 + ".");
		}
		Conclusion(sbj1); 
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int user = 1;  
		String adj1 = ""; 
		String adj2 = "";
		String sbj1 = "";
		String sbj2 = "";
		String obj1 = "";
		String obj2 = "";
		
		boolean goOn = true; 
		while (goOn) {
			if (user == 0) {
				break;
			}
			if (user == 1) {
				System.out.print("The "); 
				adj1 = Adjectives();
				System.out.print(adj1);
				adj2 = Adjectives();
				while (adj1 == adj2) {
					adj2 = Adjectives();
				}
				System.out.print(" " + adj2);
				System.out.print(" " + Subjects());
				System.out.print(" " + Verbs());
				System.out.println(" " + Objects() + ".");
				System.out.println("To quit, press 0. To generate another sentence, press 1.  If you want phase 2, press 2");
				
				user = myScanner.nextInt(); 
			}
			if (user == 2) { 
				System.out.print("The "); 
				adj1 = Adjectives();
				System.out.print(adj1);
				adj2 = Adjectives();
				while (adj1 == adj2) {
					adj2 = Adjectives();
				}
				System.out.print(" " + adj2);
				sbj1 = Subjects();
				System.out.print(" " + sbj1);
				System.out.print(" " + Verbs());
				System.out.println(" " + Objects() + ".");
				
				sbj2 = keepNoun(sbj1);
				System.out.print(sbj2);
				System.out.print(Verbs());
				obj1 = Objects();
				System.out.print(" " + obj1);
				System.out.print(" at the");
				System.out.print(" " + Adjectives());
				obj2 = Objects();
				while (obj1 == obj2) {
					obj2 = Objects();
				}
				System.out.println(" " + obj2 + ".");
				Conclusion(sbj1);
				System.out.println();
				System.out.println("Final Method:");
				finalMethod(); 
				
				System.out.println("To quit, press 0. For another sentence, press 1. For another phase 2, press 2");
				user = myScanner.nextInt(); 
			}
		}
		

	}

}       