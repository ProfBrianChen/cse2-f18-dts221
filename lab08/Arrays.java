///////////
// Dylan Scheidle
// 11/8/18 CSE 002-311
// Arrays - Creates two arrays, one with random integers 0-99, and the other to count the frequency of each integer from the first. Then the program prints the frequency of each integer.

public class Arrays{
  // main method required for every Java program
  public static void main(String[] args) {
    int[] randomNum;  // Declare array
    randomNum = new int[100];  // Set size of array
    System.out.print("Array 1 holds the following integers ");
    // Assign a random integer from 0-99 to each spot in the array
    for(int i = 0; i < 100; i++){
      randomNum[i] = (int) (Math.random()*100);
      System.out.print(randomNum[i] + " ");  // Prints the number assigned to the spot each time
    }
    System.out.println();
    int[] frequency;  // Declare array
    frequency = new int[100];  // Set size of array to 100
    // Using these three for loops and one if statement, we create another array that counts the frequency of each integer in the first array
    for (int j = 0; j < 100; j++){
      frequency[j] = j;
    }
    int count;
    for (int k = 0; k < 100; k++){
      count = 0;
      for (int n = 0; n < 100; n++){
        if (randomNum[n] == frequency[k]){
          count++;
        }
      }
      // Print the count for each integer that appeared in the first array
      if (count != 0){
        if (count == 1){
          System.out.println(k + " occurs " + count + " time");
        } else {
          System.out.println(k + " occurs " + count + " times");
        }
      }
    }
  }
}