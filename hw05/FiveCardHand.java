///////////
// Dylan Scheidle
// 10/8/18 CSE 002-311
// Five Card Hand- Asks user for input on number of trials, then prints out probability of different five card hands you can get in poker
import java.util.Scanner;
public class FiveCardHand{
                // main method required for every Java program
               public static void main(String[] args) {
                 Scanner myScanner = new Scanner( System.in ); //scanner to initiate inputs
                 boolean correct = true;
                 System.out.println("Enter the number of loops (100,1000,10000, or 1000000 suggested):");   // Queue the user to input the number of loops
                 int loopsNum = 0;   // declare variable for number of loops
                 if (myScanner.hasNextInt()){
                   loopsNum = myScanner.nextInt(); // sets input equal to variable for number of loops 
                 } else {
                   correct = false; // tests whether the input is the correct type 
                 }
                 while(!correct){ //while the type is not correct, the program will tell the user there is an error and then will reask the question until the user inputs the correct type 
                   myScanner.next();
                   System.out.println("Error: wrong type! Enter the number of loops as an interger." ); //tells user there is an error with their type and asks them for new input
                   if (myScanner.hasNextInt()){ // checking to see if input is the right type 
                     loopsNum = myScanner.nextInt(); 
                     correct = true; // if its true, it breaks out of the while loop 
                 } 
                 }
                 // Declare variables to be used throughout program.
                 int x = 1;
                 int cardNum1; 
                 int cardNum2;
                 int cardNum3;
                 int cardNum4;
                 int cardNum5;
                 int value1;
                 int value2;
                 int value3;
                 int value4;
                 int value5;
                 double four = 0;
                 double three = 0;
                 double twoPair = 0;
                 double onePair = 0;
                 
                 // Run while loop for the amount of times indicated by user
                 while (x++ <= loopsNum){
                   // Generate and assign values for each of the 5 cards
                   cardNum1 = (int)(Math.random()*52)+1;                 
                   value1 = (int)(cardNum1%13);   // assign cards 1-13 in each suit to a card value
                   
                   cardNum2 = (int)(Math.random()*52)+1;                 
                   value2 = (int)(cardNum2%13);
                   
                 if (cardNum2 != cardNum1){   // Make sure that the same number is not randomly picked more than once
                   cardNum3 = (int)(Math.random()*52)+1;                 
                   value3 = (int)(cardNum3%13);
                   
                   cardNum4 = (int)(Math.random()*52)+1;                 
                   value4 = (int)(cardNum4%13);
                   
                   cardNum5 = (int)(Math.random()*52)+1;                 
                   value5 = (int)(cardNum5%13);
                 } else {
                   cardNum2 = (int)(Math.random()*52)+1;                 
                   value2 = (int)(cardNum2%13);
                 }
                   
                   cardNum3 = (int)(Math.random()*52)+1;                 
                   value3 = (int)(cardNum3%13);
                   
                 if (cardNum3 != cardNum1){   // Make sure that the same number is not randomly picked more than once
                   cardNum4 = (int)(Math.random()*52)+1;                 
                   value4 = (int)(cardNum4%13);
                   
                   cardNum5 = (int)(Math.random()*52)+1;                 
                   value5 = (int)(cardNum5%13); 
                 } else {
                   cardNum3 = (int)(Math.random()*52)+1;                 
                   value3 = (int)(cardNum3%13);
                 }
                  
                   cardNum4 = (int)(Math.random()*52)+1;                 
                   value4 = (int)(cardNum4%13);
                   
                 if (cardNum4 != cardNum1){   // Make sure that the same number is not randomly picked more than once
                   cardNum5 = (int)(Math.random()*52)+1;                 
                   value5 = (int)(cardNum5%13); 
                 } else {
                   cardNum4 = (int)(Math.random()*52)+1;                 
                   value4 = (int)(cardNum4%13);
                 }
                   
                   cardNum5 = (int)(Math.random()*52)+1;                 
                   value5 = (int)(cardNum5%13); 
                   
                 if (cardNum5 == cardNum1){   // Make sure that the same number is not randomly picked more than once
                   cardNum5 = (int)(Math.random()*52)+1;                 
                   value5 = (int)(cardNum5%13); 
                 }
                 
                 // Count number of each hand occurring
                 if (((value1 == value2) && (value2 == value3) && (value3 == value4)) || ((value1 == value2) && (value2 == value3) && (value3 == value5)) || ((value1 == value2) && (value2 == value4) && (value4 == value5)) || ((value1 == value3) && (value3 == value4) && (value4 == value5)) || ((value2 == value3) && (value3 == value4) && (value4 == value5))){
                   four += 1;
                 } else if(((value1 == value2) && (value2 == value3)) || ((value1 == value2) && (value2 == value4)) || ((value1 == value2) && (value2 == value5)) || ((value1 == value3) && (value3 == value4)) || ((value1 == value3) && (value3 == value5)) || ((value3 == value4) && (value4 == value5)) || ((value2 == value3) && (value3 == value4)) || ((value2 == value3) && (value3 == value5)) || ((value1 == value4) && (value4 == value5)) || ((value2 == value4) && (value4 == value5))){
                   three += 1;
                 } else if (((value1 == value2) && (value3 == value4)) || ((value1 == value2) && (value3 == value5)) || ((value1 == value2) && (value4 == value5)) || ((value1 == value3) && (value2 == value5)) || ((value1 == value3) && (value2 == value4)) || ((value1 == value3) && (value4 == value5)) || ((value1 == value4) && (value2 == value3)) || ((value1 == value4) && (value2 == value5)) || ((value1 == value4) && (value3 == value5)) || ((value1 == value5) && (value2 == value3)) || ((value1 == value5) && (value2 == value4)) || ((value1 == value5) && (value3 == value4))){
                   twoPair += 1;
                 } else if ((value1 == value2) || (value1 == value3) || (value1 == value4) || (value1 == value5) || (value2 == value3) || (value2 == value4) || (value2 == value5) || (value3 == value4) || (value3 == value5) || (value4 == value5)){
                   onePair += 1;
                 }
                   
                 }       
  
                 // Divide the count of each hand by the amount of trials to find the probability of each occurring.   
                 double fourProb = four/loopsNum;
                 double threeProb = three/loopsNum;
                 double twoPairProb = twoPair/loopsNum;
                 double onePairProb = onePair/loopsNum;
                 // Print out the number of loops and the probabilities of each hand
                 System.out.println("The number of loops: " + loopsNum);
                 System.out.println("The probability of Four-of-a-kind: " + fourProb);
                 System.out.println("The probability of Three-of-a-kind: " + threeProb);
                 System.out.println("The probability of Two-pair: " + twoPairProb);
                 System.out.println("The probability of One-pair: " + onePairProb);
                 
               }
  
}