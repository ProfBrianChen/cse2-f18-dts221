//////////////
//// CSE 02 Welcome Class
/// Dylan Scheidle
/// 9/3/2018 CSE 002-311
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints Welcome, Class to terminal window
      System.out.println("  -----------");
      System.out.println("  | WELCOME |");
      System.out.println("  -----------");
      System.out.println("  ^  ^  ^  ^  ^  ^");
      System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
      System.out.println("<-D--T--S--2--2--1->");
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("  v  v  v  v  v  v");
      System.out.println("Hi, I'm Dylan Scheidle, a second year student studying Industrial and Systems Engineering and Finance through the IBE program. I enjoy fishing, lacrosse, jetskiing, and being with friends.");
  }                      
                         
}