///////////
// Dylan Scheidle
// 10/29/18 CSE 002-311
// Word Tools - Asks user to submit a sample text and then gives a prompt for a number of actions to do with the text. This program also checks to make sure the users inputs are correct.
import java.util.Random;
import java.util.Scanner;

public class WordTools{
  
  public static String sampleText(Scanner myScanner){
    String sampleText;
    System.out.println("Please type a sample text (can be multiple sentences):");
    System.out.println();
    sampleText = myScanner.nextLine();
    System.out.println("You entered: ");
    System.out.println(sampleText);
    System.out.println();
    return sampleText;
  }
  
  public static String printMenu(Scanner myScanner){
    System.out.println("MENU:");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println();
    System.out.println("Choose an option:");
    String input;
    input = myScanner.nextLine();
    return input;
  }
  
  public static int getNumOfNonWSCharacters(String sampleText){
    int counter = 0;
    char charNum;
    for (int i = 0; i < sampleText.length(); i = i+1 ){ 
      charNum = sampleText.charAt(i); 
      if(charNum != ' ') { 
          counter++; 
        }
    }
    System.out.println("Number of non-whitespace characters: " + counter );
    return counter;
  }
  
  public static int getNumOfWords(String sampleText){
    int wordCount = 0;
    boolean words = false;
    int stop = sampleText.length() - 1;
    for (int i = 0; i < sampleText.length(); i = i + 1) {
      if (sampleText.charAt(i) == '\'') { 
        wordCount--; 
      }
      if (Character.isLetter(sampleText.charAt(i)) && i != stop) { 
        words = true;
      } else if (!Character.isLetter(sampleText.charAt(i)) && words) { 
        wordCount++; 
        words = false; 
      } else if (Character.isLetter(sampleText.charAt(i)) && i == stop) { 
        wordCount++; 
      }
    }
    System.out.println("Number of word: " + wordCount);
    return wordCount; 
	}
  
  public static int findText(String sampleText){
    Scanner myScanner = new Scanner(System.in); 
    System.out.println("Type a word from the text to be found:");
    String specificWord = myScanner.nextLine(); 
    int instance = 0; 
    instance = sampleText.split(specificWord).length; 
    specificWord = "''" + specificWord + "''"; 
    System.out.print(specificWord); 
    System.out.println(" Instances: " + instance);
    return instance;
  }
  
  public static void replaceExclamation(String sampleText){
    char exclaim; 
    StringBuilder changeString = new StringBuilder(sampleText); 
    for(int i = 0; i < sampleText.length(); i = i+1 ){
      exclaim = sampleText.charAt(i);
      if (exclaim == '!') { 
        changeString.setCharAt(i,'.'); 
      }
    }
    System.out.println("Edited text: " + changeString); 
  }
  
  public static void shortenSpace(String sampleText){
    System.out.println("Edited text: " + sampleText.replaceAll("\\s+", " ")); 
  }
  
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); 
    int j = 1; 
    String sampleText = sampleText(myScanner);
    while (j == 1){
    
    String inputLetter = printMenu(myScanner);
      while (!myScanner.hasNext()){
        System.out.println("Please enter an option from the menu:");
        printMenu(myScanner);
      }
      if ((inputLetter.equals("q")) || (inputLetter.equals("c")) || (inputLetter.equals("w")) || (inputLetter.equals("f")) || (inputLetter.equals("r")) || (inputLetter.equals("s"))){
      switch (inputLetter){
        case "q":
          j = 0;
          break;
        case "c":
          getNumOfNonWSCharacters(sampleText);
          break;
        case "w":
          getNumOfWords(sampleText);
          break;
        case "f":
          findText(sampleText);
          break;
        case "r":
          replaceExclamation(sampleText);
          break;
        case "s":
          shortenSpace(sampleText);
          break;
      }
      } else {
        System.out.println("Please enter an option from the menu:");
        printMenu(myScanner);
      }
    }
  }
}