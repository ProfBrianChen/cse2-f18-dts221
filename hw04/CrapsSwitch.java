///////////
// Dylan Scheidle
// 9/24/18 CSE 002-311
// Craps Switch- Program asks user for dice inputs or to randomly select dice values and outputs slang terms of Casino game Craps.-using switch statements
import java.util.Scanner;
public class CrapsSwitch{
                // main method required for every Java program
               public static void main(String[] args) {
                 Scanner myScanner = new Scanner( System.in ); //scanner to initiate inputs
                 System.out.println("Would you like to input values of dice(1) or have dice randomly selected(2)?(1 or 2)");   // give option to user
                 int answer = myScanner.nextInt(); // scanner to accept input
                 // initialize varibales for dice
                 int dieValue1; 
                 int dieValue2;
                 
                 switch (answer){
                   case 1: answer = 1;  // ask for inputs of die values if they chose to
                     System.out.println("Input die value 1(1 to 6)");
                     dieValue1 = myScanner.nextInt();
                     System.out.println("Input die value 2(1 to 6)");
                     dieValue2 = myScanner.nextInt();
                     break;
                   default: // randomly select dice values
                     dieValue1 = (int)(Math.random()*6)+1;
                     dieValue2 = (int)(Math.random()*6)+1;
                     break;
                 }
                 
                 // assign slang terms to die combos and print outputs
                 switch (dieValue1){
                   case 1:
                     switch (dieValue2){
                      case 1:
                        System.out.println("You got Snake Eyes");
                        break;
                      case 2:
                        System.out.println("You got an Ace Deuce"); 
                        break;
                      case 3:
                        System.out.println("You got an Easy Four");
                        break;
                      case 4:                   
                        System.out.println("You got a Fever Five");
                        break;
                      case 5:
                        System.out.println("You got an Easy Six"); 
                        break;
                      case 6:
                        System.out.println("You got a Seven Out");
                        break;
                     }
                         break;
                   case 2:                
                    switch (dieValue2){
                      case 1:
                        System.out.println("You got an Ace Deuce");
                        break;
                      case 2:
                        System.out.println("You got a Hard Four"); 
                        break;
                      case 3:
                        System.out.println("You got a Fever Five");
                        break;
                      case 4:
                        System.out.println("You got an Easy Six"); 
                        break;
                      case 5:
                        System.out.println("You got a Seven Out"); 
                        break;
                      case 6:
                        System.out.println("You got an Easy Eight");
                        break;
                    }
                     break;
                   case 3:                 
                    switch (dieValue2){
                      case 1:
                        System.out.println("You got an Easy Four");
                        break;
                      case 2:
                        System.out.println("You got a Fever Five"); 
                        break;
                      case 3:
                        System.out.println("You got a Hard Six"); 
                        break;
                      case 4:
                        System.out.println("You got a Seven Out");
                        break;
                      case 5:
                        System.out.println("You got an Easy Eight"); 
                        break;
                      case 6:
                        System.out.println("You got a Nine"); 
                        break;
                    }
                     break;
                   case 4:                 
                    switch (dieValue2){
                      case 1:
                        System.out.println("You got a Fever Five");
                        break;
                      case 2:
                        System.out.println("You got an Easy Six"); 
                        break;
                      case 3:
                        System.out.println("You got a Seven Out"); 
                        break;
                      case 4:
                        System.out.println("You got a Hard Eight");
                        break;
                      case 5:
                        System.out.println("You got a Nine"); 
                        break;
                      case 6:
                        System.out.println("You got an Easy Ten"); 
                        break;
                    }
                     break;
                   case 5:
                    switch (dieValue2){
                      case 1:
                        System.out.println("You got an Easy Six");
                        break;
                      case 2:
                        System.out.println("You got a Seven Out"); 
                        break;
                      case 3:
                        System.out.println("You got an Easy Eight"); 
                        break;
                      case 4:
                        System.out.println("You got a Nine");
                        break;
                      case 5:
                        System.out.println("You got a Hard Ten"); 
                        break;
                      case 6:
                        System.out.println("You got a Yo-leven"); 
                        break;
                    }
                     break;
                   case 6:                 
                    switch (dieValue2){
                      case 1:
                        System.out.println("You got a Seven Out");
                        break;
                      case 2:
                        System.out.println("You got an Easy Eight"); 
                        break;
                      case 3:
                        System.out.println("You got a Nine"); 
                        break;
                      case 4:
                        System.out.println("You got an Easy Ten");
                        break;
                      case 5:
                        System.out.println("You got a Yo-leven"); 
                        break;
                      case 6:
                        System.out.println("You got Boxcars"); 
                        break;
                    }
                     break;
               }
               }
}