///////////
// Dylan Scheidle
// 9/24/18 CSE 002-311
// Craps If- Program asks user for dice inputs or to randomly select dice values and outputs slang terms of Casino game Craps.-using If statements
import java.util.Scanner;
public class CrapsIf{
                // main method required for every Java program
               public static void main(String[] args) {
                 Scanner myScanner = new Scanner( System.in ); //scanner to initiate inputs
                 System.out.println("Would you like to input values of dice(1) or have dice randomly selected(2)?(1 or 2)");   // give option to user
                 double answer = myScanner.nextDouble(); // scanner to accept input
                 // initialize varibales for dice
                 int dieValue1; 
                 int dieValue2;
                 
                 if (answer == 1){
                   // ask for inputs of die values if they chose to
                   System.out.println("Input die value 1(1 to 6)");
                   dieValue1 = myScanner.nextInt();
                   System.out.println("Input die value 2(1 to 6)");
                   dieValue2 = myScanner.nextInt();
                 } else {
                   // randomly select dice values
                   dieValue1 = (int)(Math.random()*6)+1;
                   dieValue2 = (int)(Math.random()*6)+1;
                 }
                 
                 // assign slang terms to die combos and print outputs
                 if (dieValue1 == 1 && dieValue2 == 1){
                   System.out.println("You got Snake Eyes");
                 } else if (dieValue1 == 1 && dieValue2 == 2){
                   System.out.println("You got an Ace Deuce"); 
                 } else if (dieValue1 == 1 && dieValue2 == 3){
                   System.out.println("You got an Easy Four"); 
                 } else if (dieValue1 == 1 && dieValue2 == 4){
                   System.out.println("You got a Fever Five"); 
                 } else if (dieValue1 == 1 && dieValue2 == 5){
                   System.out.println("You got an Easy Six"); 
                 } else if (dieValue1 == 1 && dieValue2 == 6){
                   System.out.println("You got a Seven Out"); 
                 } else if (dieValue1 == 2 && dieValue2 == 1){
                   System.out.println("You got an Ace Deuce");
                 } else if (dieValue1 == 2 && dieValue2 == 2){
                   System.out.println("You got a Hard Four"); 
                 } else if (dieValue1 == 2 && dieValue2 == 3){
                   System.out.println("You got a Fever Five"); 
                 } else if (dieValue1 == 2 && dieValue2 == 4){
                   System.out.println("You got an Easy Six"); 
                 } else if (dieValue1 == 2 && dieValue2 == 5){
                   System.out.println("You got a Seven Out"); 
                 } else if (dieValue1 == 2 && dieValue2 == 6){
                   System.out.println("You got an Easy Eight");
                 } else if (dieValue1 == 3 && dieValue2 == 1){
                   System.out.println("You got an Easy Four"); 
                 } else if (dieValue1 == 3 && dieValue2 == 2){
                   System.out.println("You got a Fever Five"); 
                 } else if (dieValue1 == 3 && dieValue2 == 3){
                   System.out.println("You got a Hard Six"); 
                 } else if (dieValue1 == 3 && dieValue2 == 4){
                   System.out.println("You got a Seven Out");
                 } else if (dieValue1 == 3 && dieValue2 == 5){
                   System.out.println("You got an Easy Eight"); 
                 } else if (dieValue1 == 3 && dieValue2 == 6){
                   System.out.println("You got a Nine"); 
                 } else if (dieValue1 == 4 && dieValue2 == 1){
                   System.out.println("You got a Fever Five"); 
                 } else if (dieValue1 == 4 && dieValue2 == 2){
                   System.out.println("You got an Easy Six"); 
                 } else if (dieValue1 == 4 && dieValue2 == 3){
                   System.out.println("You got a Seven Out"); 
                 } else if (dieValue1 == 4 && dieValue2 == 4){
                   System.out.println("You got a Hard Eight"); 
                 } else if (dieValue1 == 4 && dieValue2 == 5){
                   System.out.println("You got a Nine"); 
                 } else if (dieValue1 == 4 && dieValue2 == 6){
                   System.out.println("You got an Easy Ten");
                 } else if (dieValue1 == 5 && dieValue2 == 1){
                   System.out.println("You got an Easy Six"); 
                 } else if (dieValue1 == 5 && dieValue2 == 2){
                   System.out.println("You got a Seven Out"); 
                 } else if (dieValue1 == 5 && dieValue2 == 3){
                   System.out.println("You got an Easy Eight"); 
                 } else if (dieValue1 == 5 && dieValue2 == 4){
                   System.out.println("You got a Nine"); 
                 } else if (dieValue1 == 5 && dieValue2 == 5){
                   System.out.println("You got a Hard Ten"); 
                 } else if (dieValue1 == 5 && dieValue2 == 6){
                   System.out.println("You got a Yo-leven");
                 } else if (dieValue1 == 6 && dieValue2 == 1){
                   System.out.println("You got a Seven Out"); 
                 } else if (dieValue1 == 6 && dieValue2 == 2){
                   System.out.println("You got an Easy Eight"); 
                 } else if (dieValue1 == 6 && dieValue2 == 3){
                   System.out.println("You got a Nine"); 
                 } else if (dieValue1 == 6 && dieValue2 == 4){
                   System.out.println("You got an Easy Ten"); 
                 } else if (dieValue1 == 6 && dieValue2 == 5){
                   System.out.println("You got a Yo-leven"); 
                 } else if (dieValue1 == 6 && dieValue2 == 6){
                   System.out.println("You got Boxcars");
                    }
                 }
}
                   