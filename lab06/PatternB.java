///////////
// Dylan Scheidle
// 10/11/18 CSE 002-311
// Pattern B- Asks user for input on number of rows in a pyramid, and then prints out a pattern in the shape of a pyramid with integers
import java.util.Scanner;
public class PatternB{
                // main method required for every Java program
               public static void main(String[] args) {
                 Scanner myScanner = new Scanner( System.in ); //scanner to initiate inputs
                 boolean correct = true;
                 System.out.println("Enter the number of rows for the pyramid between 1 and 10:");   // Queue the user to input the number of rows in the pyramid for the pattern
                 int input = 0;   // declare variable for number of loops
                 if (myScanner.hasNextInt()){
                   input = myScanner.nextInt(); // sets input equal to variable for number of loops 
                 } else {
                   correct = false; // tests whether the input is the correct type 
                 }
                 while(!correct){ //while the type is not correct, the program will tell the user there is an error and then will reask the question until the user inputs the correct type 
                   myScanner.next();
                   System.out.println("Error: wrong type! Enter the number of rows in the pyramid as an integer." ); //tells user there is an error with their type and asks them for new input
                   if (myScanner.hasNextInt()){ // checking to see if input is the right type 
                     input = myScanner.nextInt(); 
                     correct = true; // if its true, it breaks out of the while loop 
                 } 
                 }
                 while (input < 1 || input > 10){
                   System.out.println("Error: integer is out of range! Enter an integer between 1 and 10.");
                   input = myScanner.nextInt();
                 }
                 int numColumns;
                  for (int numRows = input; numRows > 0; numRows--){
                   for (numColumns = 1; numColumns <= numRows; numColumns++){
                     System.out.print(numColumns + " ");
                   }
                   System.out.println();
                 }
               }
}