///////////
// Dylan Scheidle
// 9/13/18 CSE 002-311
// Check- This program will ask for a check total, percent tip, and how many people the check is being split by, calculating how much each person must pay.
import java.util.Scanner;
public class Check{
                // main method required for every Java program
               public static void main(String[] args) {
                 Scanner myScanner = new Scanner( System.in ); //scanner to initiate input
                 System.out.print("Enter the original cost of the check in the form xx.xx: "); //cue person to input check total
                 double checkCost = myScanner.nextDouble(); // scanner to accept input
                 System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //cue person to input tip percent
                 double tipPercent = myScanner.nextDouble(); //scanner to accept input
                 tipPercent /= 100; //We want to convert the percentage into a decimal value
                 System.out.print("Enter the number of people who went out to dinner: "); //cue person to input number of people who ate
                 int numPeople = myScanner.nextInt(); //scanner to accept input
                 // declare variables for calculations
                 double totalCost;
                 double costPerPerson;
                 int dollars,   //whole dollar amount of cost 
                 dimes, pennies; //for storing digits
                 //to the right of the decimal point 
                 //for the cost$ 
                 totalCost = checkCost * (1 + tipPercent); // calculation to find total cost of check with tip included
                 costPerPerson = totalCost / numPeople; // calculation to find out how much each person owes when the check is split
                 //get the whole amount, dropping decimal fraction
                 dollars=(int)costPerPerson;
                 //get dimes amount, e.g., 
                 // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
                 //  where the % (mod) operator returns the remainder
                 //  after the division:   583%100 -> 83, 27%5 -> 2 
                 dimes=(int)(costPerPerson * 10) % 10;
                 pennies=(int)(costPerPerson * 100) % 10;
                 System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //print the answer for how much each person owes
               
               }
  
}