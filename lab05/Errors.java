///////////
// Dylan Scheidle
// 10/4/18 CSE 002-311
// Errors- Program asks user for inputs of class information including instructor, time, class number, and more. Each time, program checks to see if user inputted the correct type
import java.util.Scanner;
public class Errors{
                // main method required for every Java program
               public static void main(String[] args) {
                 Scanner myScanner = new Scanner( System.in ); // scanner to initiate inputs
                 boolean correct = true; 
      
                 // Course number for class input
                 System.out.println("What is the course number of your class? "); // asks user to input course number
                 int classNum; // assigns variable to users input 
                 if (myScanner.hasNextInt()){
                   classNum = myScanner.nextInt(); // sets input equal to variable for course number 
                 } else {
                   correct = false; // tests whether the input is the correct type 
                 }
                 while(!correct){ //while the type is not correct, the program will tell the user there is an error and then will reask the question until the user inputs the correct type 
                   myScanner.next();
                   System.out.println("Error: wrong type! What is your course number?" ); //tells user there is an error with their type and asks them for new input
                   if (myScanner.hasNextInt()){ // checking to see if input is the right type 
                     classNum = myScanner.nextInt(); // sets input equal to variable for course number 
                     correct = true; // if its true, it breaks out of the while loop 
                 } 
                 }
                 
                 // Department of class
                 System.out.println("What department is your class in? "); //input department
                 String classDept;  // assigns variable to the input 
                 if (myScanner.hasNext()){
                   classDept = myScanner.next(); // sets input equal to variable 
                 } else {
                   correct = false; 
                 }
                 while(!correct){
                   myScanner.next();
                   System.out.println("Error: wrong type! What department is your class in? "); //tells user there is an error with their type and asks them for new input
                   if (myScanner.hasNext()){ // checking to see if input is the right type 
                     classDept = myScanner.next(); // sets input equal to variable 
                     correct = true; // if its true, it breaks out of the while loop 
                   } 
                 }
                 
                 // Number of times the class meets a week.
                 System.out.println("How many times does class meet per week? ");
                 int frequency;
                 if (myScanner.hasNextInt()){
                   frequency = myScanner.nextInt(); // sets input equal to variable 
                 } else {
                   correct = false; 
                 }
                 while(!correct){
                   myScanner.next();
                   System.out.println("Error: wrong type! How many times does class meet per week?" ); //tells user there is an error with their type and asks them for new input
                   if (myScanner.hasNextInt()){ // checking to see if input is the right type 
                     frequency = myScanner.nextInt(); // sets input equal to variable 
                     correct = true; // if its true, it breaks out of the while loop 
                   } 
                 }
                 
                 // Time the Class starts 
                 System.out.println("What time does your class start? ");
                 double classTime;
                 if (myScanner.hasNextDouble()){
                   classTime = myScanner.nextDouble(); // sets input equal to variable 
                 } else {
                   correct = false; // tests whether the input is the correct type 
                 }
                 while(!correct){ //while the type is not correct, the program will tell the user there is an error and then will reask the question until the user inputs the correct type 
                   myScanner.next();
                   System.out.println("Error: wrong type! What time does your class start?" ); //tells user there is an error with their type and asks them for new input
                   if (myScanner.hasNextDouble()){ // checking to see if input is the right type 
                     classTime = myScanner.nextDouble(); // sets input equal to variable for course number 
                     correct = true; // if its true, it breaks out of the while loop 
                   } 
                 }
                 
                 // Instructors name 
                 System.out.println("What is your instructors name? ");
                 String profName;
                 if (myScanner.hasNext()){
                   profName = myScanner.nextLine(); // sets input equal to variable 
                 } else {
                   correct = false; 
                 }
                 while(!correct){
                   myScanner.next();
                   System.out.println("Error: wrong type! What is your instructors name? "); //tells user there is an error with their type and asks them for new input
                   if (myScanner.hasNext()){ // checking to see if input is the right type 
                     profName = myScanner.nextLine(); // sets input equal to variable 
                     correct = true; // if its true, it breaks out of the while loop 
                   } 
                 }
                 
                 // Number of students per class
                 myScanner.next();
                 System.out.println("How many students are in the class? ");
                 int studentNum;
                 if (myScanner.hasNextInt()){
                   studentNum = myScanner.nextInt(); // sets input equal to variable 
                 } else {
                   correct = false; // tests whether the input is the correct type 
                 }
                 while(!correct){ //while the type is not correct, the program will tell the user there is an error and then will reask the question until the user inputs the correct type 
                   myScanner.next();
                   System.out.println("Error: wrong type! How many students are in the class?" ); //tells user there is an error with their type and asks them for new input
                   if (myScanner.hasNextInt()){ // checking to see if input is the right type 
                     studentNum = myScanner.nextInt(); // sets input equal to variable  
                     correct = true; // if its true, it breaks out of the while loop 
                   } 
                 }
               }
}